import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:07:01.995558
#2018-02-26 15:24:01.797521
#2018-02-26 15:48:01.309817
#2018-02-26 17:23:01.467936
#2018-02-26 18:16:01.238146
#2018-02-26 19:01:01.836398
#2018-02-26 20:16:01.475489
#2018-02-26 21:21:01.409057
#2018-02-26 22:21:01.224631
#2018-02-26 23:32:01.980355
#2018-02-27 00:17:01.313713
#2018-02-27 01:19:01.208853